# vlang-gitpod

A docker image for building vlang products using gitpod. The image uses the gitpod base and installs the latest v and vpkg from source.

## Use
To use the docker image, include a `.gitpod.yaml` file in your repo root that looks like:

```yaml
image: registry.gitlab.com/djoodle/vlang-gitpod:latest
# can also include the vscode extension for v
vscode:
  extensions:
    - 0x9ef.vscode-vlang@0.0.9:eX8ZvjVf69klCEV8iypCJg== 
```
